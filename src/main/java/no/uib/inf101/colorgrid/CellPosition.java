package no.uib.inf101.colorgrid;

// Les om records her: https://inf101.ii.uib.no/notat/mutabilitet/#record

/**
 * A CellPosition consists of a row and a column.
 *
 * @param row the row of the cell
 * @param col the column of the cell
 */
public record CellPosition(int row, int col) {

    public int getRow() {
        return 0;
    }

    public int getColumn() {
        return 0;
    }

    public double margin() {
        return 0;
    }
}
