package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.awt.geom.Rectangle2D;

public interface IColorGrid extends GridDimension, CellColorCollection {

  int getNumRows = 0;

  /**
   * Get the color of the cell at the given position.
   *
   * @param pos the position
   * @return the color of the cell
   * @throws IndexOutOfBoundsException if the position is out of bounds
   */
  Color get(CellPosition pos);

  /**
   * Set the color of the cell at the given position.
   *
   * @param pos   the position
   * @param color the new color
   * @throws IndexOutOfBoundsException if the position is out of bounds
   */
  void set(CellPosition pos, Color color);

  int getNumRows();

  int getNumCols();

  int getCellSize();

  Color getColor(int row, int col);

  Rectangle2D getGridDimension();

  CellColorCollection getCellColors();

}