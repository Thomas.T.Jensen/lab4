package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid {
  // TODO: Implement this class

  private int rows;
  private int cols;
  private ArrayList<ArrayList<CellColor>> grid;

  public ColorGrid(int rows, int cols) {
    this.rows = rows;
    this.cols = cols;
    this.grid = new ArrayList<>();

    for (int i = 0; i < rows; i++) {
      this.grid.add(new ArrayList<>());
      for (int j = 0; j < cols; j++) {
        ArrayList<CellColor> row = this.grid.get(i);
        row.add(new CellColor(new CellPosition(i, j), null));
      }
    }
  }

  @Override
  public int rows() {
    return this.rows;
  }

  @Override
  public int cols() {
    return this.cols;
  }

  @Override
  public Color get(CellPosition pos) {
    int rowindex = pos.row();
    int colindex = pos.col();

    ArrayList<CellColor> row = this.grid.get(rowindex);
    Color result = row.get(colindex).color();
    System.out.println(grid);
    return result;

  }

  @Override
  public void set(CellPosition pos, Color color) {
    int row = pos.row();
    int col = pos.col();

    ArrayList<CellColor> gridrow = grid.get(row);
    gridrow.set(col, new CellColor(pos, color));
  }

  @Override
  public List<CellColor> getCells() {
    // TODO Auto-generated method stub
    List<CellColor> cells = new ArrayList<>();
    for (ArrayList<CellColor> row : grid) {
      for (CellColor cellColor : row) {
        cells.add(cellColor);
      }
    }
    return cells;
  }

  public void setColor(int i, int j, Color yellow) {
  }

  @Override
  public int getNumColumns() {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public int getNumRows() {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public int getNumCols() {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public int getCellSize() {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public Color getColor(int row, int col) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Rectangle2D getGridDimension() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public CellPosition[] getPositions() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Color getColor(CellPosition position) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public CellColorCollection getCellColors() {
    // TODO Auto-generated method stub
    return null;
  }

}