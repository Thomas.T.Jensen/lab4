package no.uib.inf101.gridview;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.List;

import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.IColorGrid;

public class GridView extends JPanel {

  private static final double OUTERMARGIN = 30;
  private static final Color MARGINCOLOR = Color.LIGHT_GRAY;
  IColorGrid IColorGrid;

  public GridView(IColorGrid IColorGrid) {
    this.setPreferredSize(new Dimension(400, 300));
    this.IColorGrid = IColorGrid;

  }

  /*
   * (non-Javadoc)
   * 
   * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
   */

  public void drawGrid(Graphics2D g) {
    double x = OUTERMARGIN;
    double y = OUTERMARGIN;
    double width = this.getWidth() - 2 * OUTERMARGIN;
    double height = this.getHeight() - 2 * OUTERMARGIN;
    g.setColor(MARGINCOLOR);
    Rectangle2D rectangle = new Rectangle2D.Double(x, y, width, height);
    g.fill(rectangle);
    CellPositionToPixelConverter converter = new CellPositionToPixelConverter(rectangle, IColorGrid, OUTERMARGIN);
    drawCells(g, IColorGrid, converter);

  }

  private static void drawCells(Graphics2D canvas, CellColorCollection square, CellPositionToPixelConverter pos) {

    List<CellColor> list = square.getCells();
    for (CellColor cell : list) {
      Rectangle2D rectangle = pos.getBoundsForCell(cell.cellPosition());
      Color color = cell.color();
      if (cell.color() == null) {
        color = Color.DARK_GRAY;
      }
      canvas.setColor(color);
      canvas.fill(rectangle);
    }

  }

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    drawGrid(g2);
  }
  // Draw a favorite shape temporarily'

}
