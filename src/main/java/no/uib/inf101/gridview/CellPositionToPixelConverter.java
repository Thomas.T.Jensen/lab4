package no.uib.inf101.gridview;

import java.awt.geom.Rectangle2D;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;

public class CellPositionToPixelConverter {
  // TODO: Implement this class
  private Rectangle2D box;
  private GridDimension gd;
  private double margin;

  public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin) {
    this.box = box;
    this.gd = gd;
    this.margin = margin;
  }

  public Rectangle2D getBoundsForCell(CellPosition pos) {
    double Boxwidth = box.getWidth();
    double BoxHeight = box.getHeight();
    double BoxY = box.getY();
    double BoxX = box.getX();
    double cpCol = pos.col();
    double cpRow = pos.row();
    double gdCols = gd.cols();
    double GdRows = gd.rows();

    double cellWidth = (Boxwidth - (this.margin * (gdCols + 1))) / gdCols;
    double cellHeight = (BoxHeight - (this.margin * (GdRows + 1))) / GdRows;
    double CellX = (BoxX + (this.margin * (cpCol + 1)) + (cellWidth * cpCol));
    double CellY = (BoxY + (this.margin * (cpRow + 1)) + (cellHeight * cpRow));

    Rectangle2D cell = new Rectangle2D.Double(CellX, CellY, cellWidth, cellHeight);

    return cell;

  }

  public int columnToX(int column) {
    return 0;
  }

  public int rowToY(int row) {
    return 0;
  }

  public int getCellSize() {
    return 0;
  }
}
