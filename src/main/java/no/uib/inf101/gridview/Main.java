package no.uib.inf101.gridview;

import java.awt.Color;

import javax.swing.JFrame;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.ColorGrid;
import no.uib.inf101.colorgrid.IColorGrid;

public class Main {
  public static void main(String[] args) {
    // TODO: Implement this method

    // Create a new ColorGrid object with 3 rows and 4 columns
    IColorGrid grid = new ColorGrid(3, 4);

    // Set the colors of the corners
    grid.set(new CellPosition(0, 0), Color.RED);
    grid.set(new CellPosition(0, 3), Color.BLUE);
    grid.set(new CellPosition(2, 0), Color.YELLOW);
    grid.set(new CellPosition(2, 3), Color.GREEN);

    // Create a new GridView object with the ColorGrid as argument
    GridView gridView = new GridView(grid);

    // Create a new JFrame object
    JFrame frame = new JFrame();

    // Set the content pane of the JFrame to the GridView object
    frame.setContentPane(gridView);

    // Set the title of the JFrame
    frame.setTitle("INF101");

    // Set the default close operation of the JFrame
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    // Pack the components of the JFrame
    frame.pack();

    // Set the JFrame to be visible
    frame.setVisible(true);
  }
}
